PRINCIPAL= mscMonografia
LATEX= pdflatex
EPS2PDF= epstopdf
INKSCAPE= inkscape
IMAGES_SVG= images/system_arch.svg images/dummy.svg images/sistema_no_nada.svg images/sistema_no_repositorios.svg images/sistema_repositorios.svg images/sistema_repositorios_sync.svg
IMAGES_EPS= $(IMAGES_SVG:%.svg=%.eps)
IMAGES_PDF= $(IMAGES_EPS:%.eps=%.pdf)
SLIDES= presentacion

.PHONY : pdf slides clean
.SUFFIXES: .eps .pdf .svg .bbl .aux

pdf: $(IMAGES_PDF) $(PRINCIPAL).bbl
	$(LATEX) $(PRINCIPAL).tex; $(LATEX) $(PRINCIPAL).tex

slides : $(IMAGES_PDF) $(SLIDES).bbl
	$(LATEX) $(SLIDES).tex; $(LATEX) $(SLIDES).tex;

.eps.pdf:
	$(EPS2PDF) $<

.svg.eps:
	$(INKSCAPE) $< -E $@

$(PRINCIPAL).aux:
	$(LATEX) $(PRINCIPAL).tex

$(SLIDES).aux:
	$(LATEX) $(SLIDES).tex

.aux.bbl :
	bibtex $*

clean :
	rm -rf `tail -n +2 .gitignore`
